﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 饼图数据类
    /// </summary>
    public class Pie
    {
        //public Pie(int _value, string _name, ItemStyle _itemstyle)
        //{
        //    value = _value;
        //    name = _name;
        //    itemStyle = _itemstyle;
        //}

        public Pie(int _value, string _name)
        {
            value = _value;
            name = _name;
        }

        public int value { get; set; }

        public string name { get; set; }

        //public ItemStyle itemStyle { get; set; }
    }

    //public class ItemStyle {

    //    public ItemStyle(string _color)
    //    {
    //        color = _color;
    //    }
    //    public string color { get; set; }
    //}
}

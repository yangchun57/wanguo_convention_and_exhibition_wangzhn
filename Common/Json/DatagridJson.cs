﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DatagridJson<T> where T : class, new()
    {
        //private List<global::HNURMS.Models.users> list;
        //private decimal total1;

        //PagedList<T> page;
        public DatagridJson(PagedList<T> page)
        {
            total = page.TotalItemCount;
            rows = page.Data;
        }

        public DatagridJson(List<T> Rows, int Total)
        {
            rows = Rows;
            total = Total;
        }

        //public DatagridJson(List<global::HNURMS.Models.users> list, decimal total1)
        //{
        //    // TODO: Complete member initialization
        //    this.list = list;
        //    this.total1 = total1;
        //}

        //public DatagridJson(List<global::HNURMS.Models.users> list, decimal total1)
        //{
        //    // TODO: Complete member initialization
        //    this.list = list;
        //    this.total1 = total1;
        //}

        public int total { get; set; }
        public List<T> rows { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class JsonRes
    {
        public JsonRes(string _res, object _data, string _msg)
        {
            res = _res;
            data = _data;
            msg = _msg;
        }

        public string res { get; set; }
        public  object data{ get; set; }
        public string msg{ get; set; }
    }
}

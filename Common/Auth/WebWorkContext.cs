﻿using System;
using System.Collections.Generic;
using Models;

namespace Common
{
    /// <summary>
    /// PC前台工作上下文类
    /// </summary>
    public class WebWorkContext
    {
        public bool IsHttpAjax;//当前请求是否为ajax请求

        public string IP;//用户ip

        public string Url;//当前url

        public string UrlReferrer;//上一次访问的url

        public v_user UserInfo;//用户信息

        public string Btns; //按钮权限

        public string Area; //区域

        public string Controller;//控制器

        public string Action;//动作方法

        public string PageKey;//页面标示符

        public List<Tree> NavList;//导航列表

        public DateTime StartExecuteTime;//页面开始执行时间

        public double ExecuteTime;//页面执行时间

    }
}
﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Linq;
using Models;
using System;

namespace Common
{
    [PageAuth]
    public class PageBase : Controller
    {
        public WebWorkContext WorkContext = new WebWorkContext();

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.ValidateRequest = false;

            WorkContext.IsHttpAjax = WebHelper.IsAjax();
            WorkContext.IP = WebHelper.GetIP();

            WorkContext.Url = WebHelper.GetUrl();
            WorkContext.UrlReferrer = WebHelper.GetUrlReferrer();
            WorkContext.Area = WebHelper.GetArea(requestContext);

            v_user UserInfo = null;

            #region 判断用户登录信息是否为空
            if (Session["USER_INFO"] == null)
            {
                var userdata = AuthTiketHelper.GetUserData();
                if (!string.IsNullOrEmpty(userdata))
                {
                    ///获取保存在cookies的用户基本信息
                    UserInfo = JsonHelper.ToObject<v_user>(userdata);

                    #region 读取用户权限
                    //rolesusers userrole = null;
                    //using (var db = new CarEntities())
                    //{
                    //    userrole = db.rolesusers.FirstOrDefault(p => p.UserCode == UserInfo.Code);
                    //}
                    //if (userrole != null) UserInfo.rolecode = userrole.RolesCode;
                    //UserInfo.roleactions = GetRoles.GetRoleActions(UserInfo.rolecode);
                    #endregion

                    Session["USER_INFO"] = UserInfo;
                }
                else
                {
                    // 根据不同的area，返回不同的登录页面
                    switch (WorkContext.Area)
                    {
                        case "teacher":
                            Response.Redirect("~/teacher/login?from=" + WorkContext.Url);
                            break;
                        case "Admin":
                            Response.Redirect("~/admin/login?from=" + WorkContext.Url);
                            break;
                        default:
                            Response.Redirect("~/login");  // 默认登录
                            break;
                    }
                    return;
                }
            }
            else
            {
                UserInfo = (v_user)Session["USER_INFO"];
            }
            #endregion

            WorkContext.UserInfo = UserInfo;

            //设置当前控制器类名
            WorkContext.Controller = RouteData.Values["controller"].ToString().ToLower();
            //设置当前动作方法名
            WorkContext.Action = RouteData.Values["action"].ToString().ToLower();
            WorkContext.PageKey = string.Format("/{0}/{1}", WorkContext.Controller, WorkContext.Action);

        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

    }
}

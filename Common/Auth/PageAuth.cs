﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class PageAuth : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                return false;
            }

            if (httpContext.User.Identity.IsAuthenticated && base.AuthorizeCore(httpContext))
            {
                return ValidateUser();
            }

            httpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
            return false;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.HttpContext.Response.StatusCode == (int)HttpStatusCode.Forbidden)
            {
                filterContext.Result = new RedirectToRouteResult("AccessErrorPage", null);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.HttpContext.Response.Redirect(FormsAuthentication.LoginUrl);
        }

        private bool ValidateUser()
        {
            //TODO: 权限验证
            return true;
        }
    }
}

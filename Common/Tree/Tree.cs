﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Tree
    {
        /// <summary>
        /// ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 节点名称
        /// </summary>
        public string text { get; set; }
        
        /// <summary>
        /// 是否展开
        /// </summary>
        public string state  { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public string iconCls { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 待checkbox的tree，表示是否选中
        /// </summary>
        public bool @checked {get; set;}

        /// <summary>
        /// 子节点集合
        /// </summary>
        public List<Tree> children { get; set; }

        #region 默认构造函数
        /// <summary>
        /// 默认构造函数
        /// </summary>
        public Tree() 
        {
            this.children = new List<Tree>();
            this.state = "open";
        }
        #endregion

        #region 常用构造函数
        /// <summary>
        /// 常用构造函数
        /// </summary>
        public Tree(string id, string text, string state = "open", string iconCls = "", string url = "")
            : this()
        {
            this.id = id;
            this.text = text;
            this.state = state;
            this.iconCls = iconCls;
            this.url = url;
        }

        /// <summary>
        /// 常用构造函数
        /// </summary>
        public Tree(int id, string text, string state = "open", string iconCls = "", string url = "")
            : this()
        {
            this.id = id.ToString();
            this.text = text;
            this.state = state;
            this.iconCls = iconCls;
            this.url = url;
        }
        #endregion
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI;

namespace Common
{
    public class ExcelHelper
    {
        public static System.IO.MemoryStream ExportExcel<T>(string title, List<T> objList, Dictionary<string, string> nameDictionary, params string[] excelPropertyNames)
        {
            NPOI.SS.UserModel.IWorkbook workbook = new NPOI.HSSF.UserModel.HSSFWorkbook();
            NPOI.SS.UserModel.ISheet sheet = workbook.CreateSheet("Sheet1");
            NPOI.SS.UserModel.IRow row;
            NPOI.SS.UserModel.ICell cell;
            NPOI.SS.UserModel.ICellStyle cellStyle;

            int rowNum = 0;
            if (!string.IsNullOrEmpty(title))
            {
                #region 标题
                #region 标题样式
                cellStyle = workbook.CreateCellStyle();
                cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                cellStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;//垂直居中有问题
                NPOI.SS.UserModel.IFont font = workbook.CreateFont();
                font.FontHeightInPoints = 15;
                cellStyle.SetFont(font);
                #endregion
                row = sheet.CreateRow(rowNum);
                cell = row.CreateCell(0, NPOI.SS.UserModel.CellType.String);
                cell.SetCellValue(title);
                cell.CellStyle = cellStyle;
                sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, excelPropertyNames.Length > 0 ? excelPropertyNames.Length - 1 : 0));
                rowNum++;
                #endregion
            }

            if (objList.Count > 0)
            {
                Type type = objList[0].GetType();
                if (type != null)
                {
                    System.Reflection.PropertyInfo[] properties = type.GetProperties();
                    if (properties.Length > 0)
                    {
                        #region 表头
                        #region 表头样式
                        cellStyle = workbook.CreateCellStyle();
                        cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                        #endregion
                        if (excelPropertyNames.Length > 0)
                        {
                            row = sheet.CreateRow(rowNum);
                            int count = 0;
                            for (int m = 0; m < properties.Length; m++)
                            {
                                if (excelPropertyNames.Contains(properties[m].Name))
                                {
                                    cell = row.CreateCell(count, NPOI.SS.UserModel.CellType.String);
                                    string displayName = GetDisplayNameByPropertyName(properties[m].Name, nameDictionary);
                                    cell.SetCellValue(displayName == null ? "" : displayName);
                                    cell.CellStyle = cellStyle;
                                    count++;
                                }
                            }
                            rowNum++;
                        }
                        #endregion

                        #region 表体
                        if (excelPropertyNames.Length > 0)
                        {
                            for (int i = 0; i < objList.Count; i++)
                            {
                                row = sheet.CreateRow(i + rowNum);
                                int count = 0;
                                for (int j = 0; j < properties.Length; j++)
                                {
                                    if (excelPropertyNames.Contains(properties[j].Name))
                                    {
                                        cell = row.CreateCell(count);
                                        object obj = properties[j].GetValue(objList[i],null);
                                        cell.SetCellValue(obj == null ? "" : obj.ToString());
                                        cell.CellStyle = cellStyle;
                                        count++;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            workbook.Write(ms);
            return ms;
        }

        public static string GetDisplayNameByPropertyName(string propertyName, Dictionary<string, string> nameDictionary)
        {
            string result = null;
            foreach (KeyValuePair<string, string> dic in nameDictionary)
            {
                if (dic.Key == propertyName)
                {
                    result = dic.Value;
                }
                continue;
            }
            return result;
        }

    }
}

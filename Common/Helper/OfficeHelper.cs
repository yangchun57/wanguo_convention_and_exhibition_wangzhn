﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words;
using Aspose.Cells;
using Aspose.Slides;

namespace Common
{
    public class OfficeHelper
    {
        /// <summary>
        ///  word转换成pdf
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        /// <returns></returns>
        public static bool WordToPDF(string sourcePath, string targetPath)
        {
            try
            {
                var doc = new Document(sourcePath);
                doc.Save(targetPath,Aspose.Words.SaveFormat.Pdf);
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// excel转pdf
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetPath"></param>
        /// <returns></returns>
        public static bool ExcelToPDF(string sourcePath, string targetPath)
        {
            try
            {
                var excel = new Workbook(sourcePath);
                excel.Save(targetPath, Aspose.Cells.SaveFormat.Pdf);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool PptToPDF(string sourcePath, string targetPath)
        {
            try
            {
                var ppt = new Presentation(sourcePath);
                ppt.Save(targetPath, Aspose.Slides.Export.SaveFormat.Pdf);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool PptxToPDF(string sourcePath, string targetPath)
        {
            try
            {
                var ppt = new Aspose.Slides.Pptx.PresentationEx(sourcePath);
                ppt.Save(targetPath, Aspose.Slides.Export.SaveFormat.Pdf);
            }
            catch
            {
                return false;
            }

            return true;
        }

    }
}

﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common
{
    public class ZipHelper
    {
        #region bool SaveFile(string filePath, byte[] bytes) 文件保存，
        /// <summary>
        ///  文件保存，特别是有些文件放到数据库，可以直接从数据取二进制，然后保存到指定文件夹
        /// </summary>
        /// <param name="filePath">保存文件地址</param>
        /// <param name="bytes">文件二进制</param>
        /// <returns></returns>
        public static bool SaveFile(string filePath, byte[] bytes)
        {
            bool result = true;
            try
            {
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region 判断文件夹是否存在
        /// <summary>
        /// 判断文件夹是否存在
        /// </summary>
        /// <param name="path">文件夹地址</param>
        /// <returns></returns>
        public static bool directoryExist(string path)
        {
            if (!string.IsNullOrEmpty(path) && Directory.Exists(path))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region 创建文件夹
        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path">文件地址</param>
        /// <returns></returns>
        public static bool directoryAdd(string path)
        {
            if (!string.IsNullOrEmpty(path) && !Directory.Exists(path))
            {
                Directory.CreateDirectory(path); //新建文件夹  
                return true;
            }
            return false;
        }
        #endregion

        #region 获取压缩后的文件路径
        /// <summary>
        /// 获取压缩后的文件路径
        /// </summary>
        /// <param name="dirPath">压缩的文件路径</param>
        /// <param name="filesPath">多个文件路径</param>
        /// <returns></returns>
        public static string GetCompressPath(List<string> filesPath)
        {
            var zipPath = "";//返回压缩后的文件路径
            using (ZipFile zip = new ZipFile(System.Text.Encoding.Default)) //System.Text.Encoding.Default设置中文附件名称乱码，不设置会出现乱码
            {
                foreach (var file in filesPath)
                {
                    zip.AddFile(file, "");
                    //第二个参数为空，说明压缩的文件不会存在多层文件夹。比如C:\test\a\b\c.doc 压缩后解压文件会出现c.doc
                    //如果改成zip.AddFile(file);则会出现多层文件夹压缩，比如C:\test\a\b\c.doc 压缩后解压文件会出现test\a\b\c.doc
                }

                // 创建zip文件存放路径
                string dirPath = "~/Downloads/";
                dirPath = IOHelper.GetOrCreatePath(dirPath);

                zipPath = string.Format("{0}\\{1}.zip", dirPath, DateTime.Now.ToString("yyyyMMddHHmmss"));
                zip.Save(zipPath);
            }
            return zipPath;
        }
        #endregion
    }
}

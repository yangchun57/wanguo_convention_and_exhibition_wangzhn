﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Common
{
    public class AuthTiketHelper
    {

        #region 创建一个票据，放在cookie中
        /// <summary>
        /// 创建一个票据，放在cookie中
        /// 票据中的数据经过加密，解决了cookie的安全问题。
        /// </summary>
        /// <param name="username"></param>
        public static void SetCookie(string userCode, string userData)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, userCode, DateTime.Now, DateTime.Now.AddDays(1), false, userData, FormsAuthentication.FormsCookiePath);
            string encTicket = FormsAuthentication.Encrypt(ticket);
            HttpCookie newCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            HttpContext.Current.Response.Cookies.Add(newCookie);
        }
        #endregion

        #region 通过此法判断登录
        /// <summary>
        /// 通过此法判断登录
        /// </summary>
        /// <returns>已登录返回true</returns>
        public static bool IsLogin()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
        #endregion

        #region 退出登录
        /// <summary>
        /// 退出登录
        /// </summary>
        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }
        #endregion

        #region 取得登录用户名
        /// <summary>
        /// 取得登录用户名
        /// </summary>
        /// <returns></returns>
        public static string GetUserCode()
        {
            return HttpContext.Current.User.Identity.Name;
        }
        #endregion

        #region 取得票据中数据
        /// <summary>
        /// 取得票据中数据
        /// </summary>
        /// <returns></returns>
        public static string GetUserData()
        {
            try
            {
                var userdata = (HttpContext.Current.User.Identity as FormsIdentity).Ticket.UserData;
                return userdata;
            }
            catch
            {
                return "";
            }
            //return (HttpContext.Current.User.Identity as FormsIdentity).Ticket.UserData;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 列表数据，封装列表的行数据与总记录数
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Paged<T>
    {
        public Paged()
            : this(new List<T>(), 0)
        { }

        public Paged(IEnumerable<T> _rows, int _total)
        {
            rows = _rows;
            total = _total;
            pages = total / 20 + 1;
        }

        /// <summary>
        /// 获取或设置 行数据
        /// </summary>
        public IEnumerable<T> rows { get; set; }

        /// <summary>
        /// 获取或设置 数据行数
        /// </summary>
        public int total { get; set; }


        /// <summary>
        /// 总页数
        /// </summary>
        public int pages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class OrderBy<T, S> where T : class, new()
    {
        public OrderBy(Expression<Func<T, S>> _orderBy, String _orderType = "desc")
        {
            orderBy = _orderBy;
            orderType = _orderType;
        }

        /// <summary>
        /// 排序字段
        /// </summary>
        public Expression<Func<T, S>> orderBy;

        /// <summary>
        /// 排序类型，升序、或降序
        /// </summary>
        public String orderType;
    }

    /// <summary>
    /// 传入排序字段名，及排序类型，对应LinqHelper的DataSorting方法
    /// </summary>
    public class OrderBy2
    {
        public OrderBy2(string orderby, string ordertype)
        {
            orderBy = orderby;
            orderType = ordertype;
        }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string orderBy;

        /// <summary>
        /// 排序类型，升序、或降序
        /// </summary>
        public string orderType;
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 适用于DataTable控件的Paged类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataTablePaged<T>
    {
        public DataTablePaged(DataTablesParameters dataTablesParameters, IEnumerable<T> rowsEnumerable, int recordsTotalParam)
        {
            var rowslist = rowsEnumerable as IList<T> ?? rowsEnumerable.ToList();

            draw = dataTablesParameters.Draw;
            recordsTotal = rowslist.Count();
            recordsFiltered = recordsTotalParam; 
            data = rowslist;
        }

        public DataTablePaged(string errorParam)
        {
            error = errorParam;
        }

        /// <summary>
        /// 请求次数计数器，每次发送给服务器后又原封返回.
        /// </summary>
        public int draw { get; set; }

        /// <summary>
        /// 即没有过滤的记录数（数据库里总共记录数）
        /// </summary>
        public int recordsTotal { get; set; }

        /// <summary>
        /// 过滤后的记录数
        /// </summary>
        public int recordsFiltered { get; set; }

        /// <summary>
        /// 表中中需要显示的数据。
        /// </summary>
        public IEnumerable<T> data { get; set; }

        /// <summary>
        /// 可选。你可以定义一个错误来描述服务器出了问题后的友好提示
        /// </summary>
        public string error { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
    public partial class v_user
    {
        /// <summary>
        /// 加密后的Code
        /// </summary>
        public string SecCode { get; set; }

        public string Description { get; set; }
    }
}

﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBLL
{
    public partial interface IcejlxxBLL : IBaseBLL<Models.cejlxx>
    {
    }

    public partial interface IclicklogBLL : IBaseBLL<Models.clicklog>
    {
    }

    public partial interface IcontentBLL : IBaseBLL<Models.content>
    {
    }

    public partial interface IcontenttypeBLL : IBaseBLL<Models.contenttype>
    {
    }

    public partial interface IunitsBLL : IBaseBLL<Models.units>
    {
    }

    public partial interface IuserBLL : IBaseBLL<Models.user>
    {
    }

    public partial interface IuserextBLL : IBaseBLL<Models.userext>
    {
    }

    public partial interface Iv_userBLL : IBaseBLL<Models.v_user>
    {
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;

namespace Web.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Admin/Login/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 生成图片验证码
        /// </summary>
        /// <returns></returns>
        public ActionResult GetImgVCode()
        {
            var imgvcode = new ImgVCodeHelper();
            Session["ImgVCode"] = imgvcode.Text;
            return File(imgvcode.ByteImge, "image/jpeg");
        }

    }
}

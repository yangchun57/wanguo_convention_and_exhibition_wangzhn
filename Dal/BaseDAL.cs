﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Models;
using Common;

namespace DAL
{
    /// <summary>
    /// 数据层 父类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseDAL<T> : IDAL.IBaseDAL<T> where T : class,new()
    {
        /// <summary>
        /// EF上下文对象
        /// </summary>
        DbContext db = new hnujlEntities(); //new DBContextFactory().GetDbContext();// 

        #region 1.0 新增 实体 +int Add(T model)
        /// <summary>
        /// 新增 实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Add(T model)
        {
            try
            {
                db.Set<T>().Add(model);
                return db.SaveChanges();//保存成功后，会将自增的id设置给 model的 主键属性，并返回受影响行数
            }
            catch
            {
                return -1;
            }
        }
        #endregion

        #region 2.0 根据 id 删除 +int Del(T model)
        /// <summary>
        /// 根据 id 删除
        /// </summary>
        /// <param name="model">包含要删除id的对象</param>
        /// <returns></returns>
        public int Del(T model)
        {
            db.Set<T>().Attach(model);
            db.Set<T>().Remove(model);
            return db.SaveChanges();
        }
        #endregion

        #region 3.0 根据条件删除 +int DelBy(Expression<Func<T, bool>> delWhere)
        /// <summary>
        /// 3.0 根据条件删除
        /// </summary>
        /// <param name="delWhere"></param>
        /// <returns></returns>
        public int DelBy(Expression<Func<T, bool>> delWhere)
        {
            //3.1查询要删除的数据
            List<T> listDeleting = db.Set<T>().Where(delWhere).ToList();
            //3.2将要删除的数据 用删除方法添加到 EF 容器中
            listDeleting.ForEach(u =>
            {
                db.Set<T>().Attach(u);//先附加到 EF容器
                db.Set<T>().Remove(u);//标识为 删除 状态
            });
            //3.3一次性 生成sql语句到数据库执行删除
            return db.SaveChanges();
        }
        #endregion

        #region 4.0 修改 +int Modify(T model, params string[] proNames)
        /// <summary>
        /// 4.0 修改，如：
        /// T u = new T() { uId = 1, uLoginName = "asdfasdf" };
        /// this.Modify(u, "uLoginName");
        /// </summary>
        /// <param name="model">要修改的实体对象</param>
        /// <param name="proNames">要修改的 属性 名称</param>
        /// <returns></returns>
        public int Modify(T model, params string[] proNames)
        {
            //4.1将 对象 添加到 EF中
            DbEntityEntry entry = db.Entry<T>(model);
            //4.2先设置 对象的包装 状态为 Unchanged
            entry.State = System.Data.Entity.EntityState.Unchanged;
            //4.3循环 被修改的属性名 数组
            foreach (string proName in proNames)
            {
                //4.4将每个 被修改的属性的状态 设置为已修改状态;后面生成update语句时，就只为已修改的属性 更新
                entry.Property(proName).IsModified = true;
            }
            //4.4一次性 生成sql语句到数据库执行
            return db.SaveChanges();
        }
        #endregion

        #region 4.0 批量修改 +int Modify(T model, Expression<Func<T, bool>> whereLambda, params string[] modifiedProNames)
        /// <summary>
        /// 4.0 批量修改
        /// </summary>
        /// <param name="model">要修改的实体对象</param>
        /// <param name="whereLambda">查询条件</param>
        /// <param name="proNames">要修改的 属性 名称</param>
        /// <returns></returns>
        public int ModifyBy(T model, Expression<Func<T, bool>> whereLambda, params string[] modifiedProNames)
        {
            //4.1查询要修改的数据
            List<T> listModifing = db.Set<T>().Where(whereLambda).ToList();

            //获取 实体类 类型对象
            Type t = typeof(T); // model.GetType();
            //获取 实体类 所有的 公有属性
            List<PropertyInfo> proInfos = t.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();
            //创建 实体属性 字典集合
            Dictionary<string, PropertyInfo> dictPros = new Dictionary<string, PropertyInfo>();
            //将 实体属性 中要修改的属性名 添加到 字典集合中 键：属性名  值：属性对象
            proInfos.ForEach(p =>
            {
                if (modifiedProNames.Contains(p.Name))
                {
                    dictPros.Add(p.Name, p);
                }
            });

            //4.3循环 要修改的属性名
            foreach (string proName in modifiedProNames)
            {
                //判断 要修改的属性名是否在 实体类的属性集合中存在
                if (dictPros.ContainsKey(proName))
                {
                    //如果存在，则取出要修改的 属性对象
                    PropertyInfo proInfo = dictPros[proName];
                    //取出 要修改的值
                    object newValue = proInfo.GetValue(model, null); //object newValue = model.uName;

                    //4.4批量设置 要修改 对象的 属性
                    foreach (T usrO in listModifing)
                    {
                        //为 要修改的对象 的 要修改的属性 设置新的值
                        proInfo.SetValue(usrO, newValue, null); //usrO.uName = newValue;
                    }
                }
            }
            //4.4一次性 生成sql语句到数据库执行
            return db.SaveChanges();
        }
        #endregion

        #region 5.0 根据条件查询 +List<T> GetListBy(Expression<Func<T,bool>> whereLambda)
        /// <summary>
        /// 5.0 根据条件查询 +List<T> GetListBy(Expression<Func<T,bool>> whereLambda)
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public List<T> GetListBy(Expression<Func<T, bool>> whereLambda)
        {
            return whereLambda == null ? db.Set<T>().ToList() : db.Set<T>().Where(whereLambda).ToList();
        }
        #endregion

        #region 5.1 根据条件 排序 和查询 + List<T> GetListBy<TKey>
        /// <summary>
        /// 5.1 根据条件 排序 和查询
        /// </summary>
        /// <typeparam name="TKey">排序字段类型</typeparam>
        /// <param name="whereLambda">查询条件 lambda表达式</param>
        /// <param name="orderLambda">排序条件 lambda表达式</param>
        /// <returns></returns>
        public List<T> GetListBy<TKey>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderLambda)
        {
            return whereLambda == null ? db.Set<T>().OrderBy(orderLambda).ToList() : db.Set<T>().Where(whereLambda).OrderBy(orderLambda).ToList();
        }
        #endregion

        #region 5.2 根据条件组、排序字段组，进行查询、排序
        /// <summary>
        /// 5.2 根据条件组、排序字段组，进行查询、排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereLambdas"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public List<T> GetListByConditions<TKey>(List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy<T, TKey>> orderBy)
        {
            var allItems = db.Set<T>() as IQueryable<T>;

            if (whereLambdas != null)
            {
                foreach (var item in whereLambdas)
                {
                    allItems = allItems.Where(item);
                }
            }

            #region 加载排序条件组，判断判断条件类型，顺序、或逆序
            if (orderBy != null)
            {
                foreach (var item in orderBy)
                {
                    switch (item.orderType)
                    {
                        case "asc":
                            allItems = allItems.OrderBy(item.orderBy);
                            break;
                        case "desc":
                            allItems = allItems.OrderByDescending(item.orderBy);
                            break;
                        default:
                            break;
                    }
                }
            }
            #endregion

            return allItems.ToList();

        }
        #endregion

        #region 5.2 根据条件查询对象 + T Find(Expression<Func<T, bool>> whereLambda)
        /// <summary>
        /// 5.2 根据条件查询对象
        /// </summary>
        /// <param name="whereLambda">查询条件 lambda表达式</param>
        /// <returns></returns>
        public T Find(Expression<Func<T, bool>> whereLambda)
        {
            return db.Set<T>().Where(whereLambda).FirstOrDefault();
        }
        #endregion

        #region 6.0 分页查询 + List<T> GetPagedList<TKey>
        /// <summary>
        /// 6.0 分页查询 + List<T> GetPagedList<TKey>
        /// </summary>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <param name="whereLambda">条件 lambda表达式</param>
        /// <param name="orderBy">排序 lambda表达式</param>
        /// <returns></returns>
        public Paged<T> GetPagedList<TKey>(int page, int rows, Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderBy)
        {
            var allItems = (whereLambda == null) ? db.Set<T>() : db.Set<T>().Where(whereLambda);
            var total = allItems.Count();
            var items = allItems.OrderByDescending(orderBy).Skip((page - 1) * rows).Take(rows);

            // 分页 一定注意： Skip 之前一定要 OrderBy
            //return db.Set<T>().Where(whereLambda).OrderBy(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return new Paged<T>(items, total);
        }
        #endregion

        #region 6.1 分页查询（条件组） + List<T> GetPagedList<TKey>
        /// <summary>
        /// 6.1 分页查询（条件组） + List<T> GetPagedList<TKey>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">页容量</param>
        /// <param name="whereLambdas">条件组 lambda表达式</param>
        /// <param name="orderBy">排序 lambda表达式</param>   ToDo 后续可以调整为“条件组”形式
        /// <returns></returns>
        public Paged<T> GetPagedList2<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, Expression<Func<T, TKey>> orderBy)
        {
            var allItems = db.Set<T>() as IQueryable<T>;

            if(whereLambdas != null)
            {
                foreach (var item in whereLambdas)
                {
                    allItems = allItems.Where(item);
                }
            }

            var total = allItems.Count();
            var items = allItems.OrderByDescending(orderBy).Skip((page - 1) * rows).Take(rows); // 分页 一定注意： Skip 之前一定要 OrderBy
            return new Paged<T>(items, total);
        }

        public Paged<T> GetPagedList2<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, Expression<Func<T, TKey>> orderBy, string sortOrder = "asc")
        {
            var allItems = db.Set<T>() as IQueryable<T>;

            if (whereLambdas != null)
            {
                foreach (var item in whereLambdas)
                {
                    allItems = allItems.Where(item);
                }
            }

            #region 判断判断条件类型，顺序、或逆序
            switch (sortOrder)
            {
                case "asc":
                    allItems = allItems.OrderBy(orderBy);
                    break;
                case "desc":
                    allItems = allItems.OrderByDescending(orderBy);
                    break;
                default:
                    break;
            }
            #endregion

            var total = allItems.Count();
            var items = allItems.Skip((page - 1) * rows).Take(rows); // 分页 一定注意： Skip 之前一定要 OrderBy
            return new Paged<T>(items, total);
        }
        #endregion

        #region 6.2 分页查询（查询条件组、排序条件组） + List<T> GetPagedList<TKey>
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="whereLambdas"></param>
        /// <param name="orderBy">排序条件</param>
        /// <param name="sortOrder">排序方式</param>
        /// <returns></returns>
        public Paged<T> GetPagedList3<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy<T, TKey>> orderBy)
        {
            var allItems = db.Set<T>() as IQueryable<T>;

            if (whereLambdas != null)
            {
                foreach (var item in whereLambdas)
                {
                    allItems = allItems.Where(item);
                }
            }

            #region 加载排序条件组，判断判断条件类型，顺序、或逆序
            if(orderBy != null)
            {
                foreach(var item in orderBy)
                {
                    switch (item.orderType)
                    {
                        case "asc":
                            allItems = allItems.OrderBy(item.orderBy);
                            break;
                        case "desc":
                            allItems = allItems.OrderByDescending(item.orderBy);
                            break;
                        default:
                            break;
                    }
                }
            }
            #endregion

            var total = allItems.Count();
            var items = allItems.Skip((page - 1) * rows).Take(rows); // 分页 一定注意： Skip 之前一定要 OrderBy
            return new Paged<T>(items, total);
        }
        #endregion

        #region 6.3 分页查询 DataTable控件使用 DataTablePaged<T> GetDataTablePaged<TKey>

        /// <summary>
        /// 6.3 分页查询 DataTable控件使用 DataTablePaged<T> GetDataTablePaged<TKey>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="dataTablesParameters"></param>
        /// <param name="whereLambdas"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public DataTablePaged<T> GetDataTablePagedList(DataTablesParameters dataTablesParameters,
            List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy2> orderBy)
        {
            var allItems = db.Set<T>() as IQueryable<T>;

            #region 加载查询条件
            if (whereLambdas != null)
            {
                foreach (var item in whereLambdas)
                {
                    allItems = allItems.Where(item);
                }
            }
            #endregion

            #region 加载排序条件组，判断判断条件类型，顺序、或逆序
            if (dataTablesParameters.Order.Count > 0)
            {
                allItems = LinqHelper.DataSorting(allItems, dataTablesParameters.OrderBy, dataTablesParameters.OrderDir.ToString());
            }
            else if (orderBy.Count > 0) //使用默认的排序条件
            {
                foreach (var item in orderBy)
                {
                    allItems = LinqHelper.DataSorting(allItems, item.orderBy, item.orderType);
                }
            }
            #endregion

            var iTotalRecords = allItems.Count();
            var items = dataTablesParameters.Length > -1
                ? allItems.Skip(dataTablesParameters.Start).Take(dataTablesParameters.Length)
                : allItems; // 分页 一定注意： Skip 之前一定要 OrderBy；当Length为-1时，表示查询全部记录，不做分页
            return new DataTablePaged<T>(dataTablesParameters, items, iTotalRecords);

        }
        #endregion

    }
}
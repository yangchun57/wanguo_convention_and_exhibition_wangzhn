﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDAL
{
	public partial interface IcejlxxDAL : IBaseDAL<Models.cejlxx>
    {
    }

	public partial interface IclicklogDAL : IBaseDAL<Models.clicklog>
    {
    }

	public partial interface IcontentDAL : IBaseDAL<Models.content>
    {
    }

	public partial interface IcontenttypeDAL : IBaseDAL<Models.contenttype>
    {
    }

	public partial interface IunitsDAL : IBaseDAL<Models.units>
    {
    }

	public partial interface IuserDAL : IBaseDAL<Models.user>
    {
    }

	public partial interface IuserextDAL : IBaseDAL<Models.userext>
    {
    }

	public partial interface Iv_userDAL : IBaseDAL<Models.v_user>
    {
    }


}
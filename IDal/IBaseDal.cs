﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace IDAL
{
    /// <summary>
    /// 数据层父接口
    /// </summary>
    public interface IBaseDAL<T> where T : class ,new()
    {
        //定义增删改查方法
        #region 1.0 新增 实体 +int Add(T model)
        /// <summary>
        /// 新增 实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Add(T model);
        #endregion

        #region 2.0 根据 id 删除 +int Del(T model)
        /// <summary>
        /// 根据 id 删除
        /// </summary>
        /// <param name="model">包含要删除id的对象</param>
        /// <returns></returns>
        int Del(T model);
        #endregion

        #region 3.0 根据条件删除 +int DelBy(Expression<Func<T, bool>> delWhere)
        /// <summary>
        /// 3.0 根据条件删除
        /// </summary>
        /// <param name="delWhere"></param>
        /// <returns></returns>
        int DelBy(Expression<Func<T, bool>> delWhere);
        #endregion

        #region 4.0 修改 +int Modify(T model, params string[] proNames)
        /// <summary>
        /// 4.0 修改，如：
        /// T u = new T() { uId = 1, uLoginName = "asdfasdf" };
        /// this.Modify(u, "uLoginName");
        /// </summary>
        /// <param name="model">要修改的实体对象</param>
        /// <param name="proNames">要修改的 属性 名称</param>
        /// <returns></returns>
        int Modify(T model, params string[] proNames);
        #endregion

        #region 4.0 批量修改 +int Modify(T model, Expression<Func<T, bool>> whereLambda, params string[] modifiedProNames)
        /// <summary>
        /// 4.0 批量修改
        /// </summary>
        /// <param name="model">要修改的实体对象</param>
        /// <param name="whereLambda">查询条件</param>
        /// <param name="proNames">要修改的 属性 名称</param>
        /// <returns></returns>
        int ModifyBy(T model, Expression<Func<T, bool>> whereLambda, params string[] modifiedProNames);
        #endregion

        #region 5.0 根据条件查询 +List<T> GetListBy(Expression<Func<T,bool>> whereLambda)
        /// <summary>
        /// 5.0 根据条件查询 +List<T> GetListBy(Expression<Func<T,bool>> whereLambda)
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        List<T> GetListBy(Expression<Func<T, bool>> whereLambda);
        #endregion

        #region 5.1 根据条件 排序 和查询 + List<T> GetListBy<TKey>
        /// <summary>
        /// 5.1 根据条件 排序 和查询
        /// </summary>
        /// <typeparam name="TKey">排序字段类型</typeparam>
        /// <param name="whereLambda">查询条件 lambda表达式</param>
        /// <param name="orderLambda">排序条件 lambda表达式</param>
        /// <returns></returns>
        List<T> GetListBy<TKey>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderLambda);
        #endregion

        #region 5.2 根据条件组、排序字段组，进行查询、排序
        /// <summary>
        /// 5.2 根据条件组、排序字段组，进行查询、排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereLambdas"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        List<T> GetListByConditions<TKey>(List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy<T, TKey>> orderBy);
        #endregion

        #region 5.2 根据条件查询对象 + T Find(Expression<Func<T, bool>> whereLambda)
        /// <summary>
        /// 5.2 根据条件查询对象
        /// </summary>
        /// <param name="whereLambda">查询条件 lambda表达式</param>
        /// <returns></returns>
        T Find(Expression<Func<T, bool>> whereLambda);
        #endregion

        #region 6.0 分页查询 + List<T> GetPagedList<TKey>
        /// <summary>
        /// 6.0 分页查询 + List<T> GetPagedList<TKey>
        /// </summary>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <param name="whereLambda">条件 lambda表达式</param>
        /// <param name="orderBy">排序 lambda表达式</param>
        /// <returns></returns>
        Paged<T> GetPagedList<TKey>(int page, int rows, Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderBy); 
        #endregion

        #region 6.1 分页查询（条件组） + List<T> GetPagedList<TKey>
        /// <summary>
        /// 6.1 分页查询（条件组） + List<T> GetPagedList<TKey>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">页容量</param>
        /// <param name="whereLambdas">条件组 lambda表达式</param>
        /// <param name="orderBy">排序 lambda表达式</param>
        /// <returns></returns>
        Paged<T> GetPagedList2<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, Expression<Func<T, TKey>> orderBy);

        Paged<T> GetPagedList2<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, Expression<Func<T, TKey>> orderBy, string sortOrder = "asc");
        #endregion

        #region 6.2 分页查询（查询条件组、排序条件组） + List<T> GetPagedList<TKey>
        Paged<T> GetPagedList3<TKey>(int page, int rows, List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy<T, TKey>> orderBy);
        #endregion

        #region 6.3 分页查询 DataTable控件使用 DataTablePaged<T> GetDataTablePaged<TKey>
        /// <summary>
        /// 6.3 分页查询 DataTable控件使用 DataTablePaged<T> GetDataTablePaged<TKey>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="dataTablesParameters"></param>
        /// <param name="whereLambdas"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        DataTablePaged<T> GetDataTablePagedList(DataTablesParameters dataTablesParameters
            , List<Expression<Func<T, bool>>> whereLambdas, List<OrderBy2> orderBy);
        #endregion
    }
}
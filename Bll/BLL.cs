﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBLL;
using DAL;

namespace BLL
{
    public partial class cejlxxBLL : BaseBLL<Models.cejlxx>, IcejlxxBLL
    {
		public override void SetDAL()
		{
			idal = new cejlxxDAL();
		}
    }

    public partial class clicklogBLL : BaseBLL<Models.clicklog>, IclicklogBLL
    {
		public override void SetDAL()
		{
			idal = new clicklogDAL();
		}
    }

    public partial class contentBLL : BaseBLL<Models.content>, IcontentBLL
    {
		public override void SetDAL()
		{
			idal = new contentDAL();
		}
    }

    public partial class contenttypeBLL : BaseBLL<Models.contenttype>, IcontenttypeBLL
    {
		public override void SetDAL()
		{
			idal = new contenttypeDAL();
		}
    }

    public partial class unitsBLL : BaseBLL<Models.units>, IunitsBLL
    {
		public override void SetDAL()
		{
			idal = new unitsDAL();
		}
    }

    public partial class userBLL : BaseBLL<Models.user>, IuserBLL
    {
		public override void SetDAL()
		{
			idal = new userDAL();
		}
    }

    public partial class userextBLL : BaseBLL<Models.userext>, IuserextBLL
    {
		public override void SetDAL()
		{
			idal = new userextDAL();
		}
    }

    public partial class v_userBLL : BaseBLL<Models.v_user>, Iv_userBLL
    {
		public override void SetDAL()
		{
			idal = new v_userDAL();
		}
    }


}
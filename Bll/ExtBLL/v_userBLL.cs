﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Common;
using CodeHelper = Models.CodeHelper;

namespace BLL
{
    public partial class v_userBLL
    {
        #region 通过加密的Code，获取用户信息
        /// <summary>
        /// 通过加密的Code，获取用户信息
        /// </summary>
        /// <param name="seccode">加密后的用户编号</param>
        /// <param name="sessionid"></param>
        /// <returns></returns>
        public v_user FindBySecCode(string seccode, string sessionid = "", string ip = "")
        {
            // 防止加密字符串解析时出错
            try
            {
                var code = SecureHelper.AESDecrypt(seccode);
                var vuser = this.Find(p => p.Code == code);
                if (vuser == null) return vuser;
                vuser.SecCode = seccode;
                
                #region 更新统计点击次数
                using (var db = new hnujlEntities())
                {
                    var user = db.user.FirstOrDefault(p => p.Code == code);
                    #region 增加点击记录
                    // 增加点击记录
                    db.clicklog.Add(new clicklog()
                    {
                        Code = CodeHelper.GetCode(),
                        UserCode = user.Code,
                        SessionID = sessionid,
                        IP = ip,
                        AddTime = System.DateTime.Now
                    });
                    #endregion

                    #region 统计最新点击次数
                    // 统计最新点击次数
                    var listClickNum = (from t in db.clicklog
                                              where t.UserCode == code
                                              group t by new
                                              {
                                                  t.UserCode,
                                                  t.SessionID
                                              }
                                                  into m
                                                  select new
                                                  {
                                                      AdviceCode = m.Key.UserCode,
                                                      Count = m.Count()
                                                  }).ToList();
                    #endregion

                    // 更新点击次数
                    user.ClickNum = listClickNum.Count + 1;
                    db.SaveChanges();

                }
                #endregion

                return vuser;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region 通过用户编号，获取用户信息
        /// <summary>
        /// 通过用户编号，获取用户信息
        /// </summary>
        /// <param name="code"></param>
        /// <param name="sessionid"></param>
        /// <returns></returns>
        public v_user FindByCode(string code,string sessionid = "", string ip = "")
        {
            if (string.IsNullOrEmpty(code)) return null;

            var _user = this.Find(p => p.Code == code);
            if (_user == null) return _user;
            _user.SecCode = SecureHelper.AESEncrypt(_user.Code);

            #region 更新统计点击次数
            using (var db = new hnujlEntities())
            {
                var user = db.user.FirstOrDefault(p => p.Code == code);
                #region 增加点击记录
                // 增加点击记录
                db.clicklog.Add(new clicklog()
                {
                    Code = CodeHelper.GetCode(),
                    UserCode = user.Code,
                    SessionID = sessionid,
                    IP = ip,
                    AddTime = System.DateTime.Now
                });
                #endregion

                #region 统计最新点击次数
                // 统计最新点击次数
                var listClickNum = (from t in db.clicklog
                                    where t.UserCode == code
                                    group t by new
                                    {
                                        t.UserCode,
                                        t.SessionID
                                    }
                                        into m
                                        select new
                                        {
                                            AdviceCode = m.Key.UserCode,
                                            Count = m.Count()
                                        }).ToList();
                #endregion

                // 更新点击次数
                user.ClickNum = listClickNum.Count + 1;
                db.SaveChanges();

            }
            #endregion

            return _user;
        }
        #endregion

    }
}
